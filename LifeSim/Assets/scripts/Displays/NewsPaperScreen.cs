﻿using UnityEngine;
using System.Collections;

public class NewsPaperScreen : MonoBehaviour {


	// pages
	public GameObject page1;
	public GameObject page2;
	public GameObject page3;
	//clickedbuttons
	public GameObject page1Clicked;
	public GameObject page2Clicked;
	public GameObject page3Clicked;


	// Use this for initialization
	void Awake () {
		gameObject.SetActive (false);

		page1.SetActive (true);
		page2.SetActive (false);
		page3.SetActive (false);
		page1Clicked.SetActive (true);
		page2Clicked.SetActive (false);
		page3Clicked.SetActive (false);
	}

	
	// Update is called once per frame
	void Update () {
	
		//set the clicked button page

		//page 1
		if (page1.activeInHierarchy) {
			page1Clicked.SetActive (true);
		} else {
			page1Clicked.SetActive (false);
		}
		//page 2
		if (page2.activeInHierarchy) {
			page2Clicked.SetActive (true);
		} else {
			page2Clicked.SetActive (false);
		}
		//page3
		if (page3.activeInHierarchy) {
			page3Clicked.SetActive (true);
		} else {
			page3Clicked.SetActive (false);
		}
	}


	// click functions


	public void GoToPage1 (){
		if (page1.activeInHierarchy==false) {
			page1.SetActive (true);
			page2.SetActive (false);
			page3.SetActive (false);

		}
	}

	public void GoToPage2 (){
		if (page2.activeInHierarchy==false) {
			page2.SetActive (true);
			page1.SetActive (false);
			page3.SetActive (false);

		}
	}

	public void GoToPage3 (){
		if (page3.activeInHierarchy==false) {
			page3.SetActive (true);
			page1.SetActive (false);
			page2.SetActive (false);

		}
	}
}
