﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {

	public Animator animator;

	// Use this for initialization
	void Start () {

		AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo (0);
		Destroy (gameObject, clipInfo [0].clip.length);
	}

	public void SetAlertText(string data){
		animator.GetComponent<Text>().text = data;
	}
	

}
