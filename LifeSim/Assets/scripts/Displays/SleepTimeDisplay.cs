﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SleepTimeDisplay : MonoBehaviour {


	public static float sleepForTime;
	private Text text;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text> () as Text;
		
	}
	
	// Update is called once per frame
	void Update () {
		text.text = sleepForTime.ToString ();
	}

	public void SliderSleepFor (float sleepForTimeNew){
		sleepForTime = sleepForTimeNew;
	
	}
		
}
