﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyDisplay : MonoBehaviour {

	private Text text;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text> () as Text;
	}
	
	// Update is called once per frame
	void Update () {
		
		text.text = "$" + Stats.money.ToString ();
	}
}
