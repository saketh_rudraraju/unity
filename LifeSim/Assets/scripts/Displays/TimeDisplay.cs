﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeDisplay : MonoBehaviour {
	
	public TimeMachine timeMachine;
	public string test; 
	private Text timeText;

	// Use this for initialization
	void Start () {
		timeMachine = GameObject.FindGameObjectWithTag ("TimeMachine").GetComponent<TimeMachine>();
		timeText = GetComponent<Text> () as Text;
	}
	
	// Update is called once per frame
	void Update () {
		timeText.text = TimeMachine.hourDTime.ToString("00") + ":" + TimeMachine.minuteDTime.ToString("00");

	}
}
