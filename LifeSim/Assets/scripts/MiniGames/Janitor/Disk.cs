﻿using UnityEngine;
using System.Collections;

public class Disk : MonoBehaviour {


	private Rigidbody2D rgbd2D;


	// Use this for initialization
	void Start () {
	
		rgbd2D = this.GetComponent<Rigidbody2D> ();

	}
	
	// Update is called once per frame

	void Update () {

			
	}

	void OnCollisionEnter2D (Collision2D collision){
		Vector2 tweak = new Vector2 (Random.Range (0f, 100f), Random.Range (0f, 100f));
		rgbd2D.velocity += tweak;

	}
}
