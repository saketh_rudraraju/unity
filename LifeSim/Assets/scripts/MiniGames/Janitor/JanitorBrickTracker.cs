﻿using UnityEngine;
using System.Collections;

public class JanitorBrickTracker : MonoBehaviour {

	public static int numberOfGums;
	private LevelManager levelmanager;
	// Use this for initialization
	void Start () {
	
		levelmanager = FindObjectOfType<LevelManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (numberOfGums <= 0) {
			levelmanager.loadLevel ("Win Janitor");

		}
	}
}
