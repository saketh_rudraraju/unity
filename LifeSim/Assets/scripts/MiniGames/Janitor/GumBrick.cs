﻿using UnityEngine;
using System.Collections;

public class GumBrick : MonoBehaviour {


	public int maxHits;
	private int timesHit;
	public Sprite[] hitSprites;
	private LevelManager levelManager;
	// Use this for initialization

	void Start () {
		
		JanitorBrickTracker.numberOfGums++;

		//get levelmanager
		levelManager = GameObject.FindObjectOfType<LevelManager>();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (timesHit >= maxHits) {
			 JanitorBrickTracker.numberOfGums--;
			Destroy (gameObject);
		} else {
			LoadSprites ();
		}

	
	}

	void OnCollisionEnter2D(Collision2D collison){

		timesHit++;

	}


	void LoadSprites(){

		int spriteIndex = maxHits - 1;
		this.GetComponent<SpriteRenderer> ().sprite = hitSprites [spriteIndex];

	}
}
