﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



	
	public class Stats : MonoBehaviour {

	public static Stats statsinstance;

	public static string home;
	public static string job;
	public static float money;
	public static float health;
	public static float mood;
	public static float energy;

	public float healthdebug;


	// Use this for initialization

	void Awake () {


		if (statsinstance == null) {
			GameObject.DontDestroyOnLoad (gameObject);
			statsinstance = this;
		} else if (statsinstance != this) {
			Destroy (gameObject);
		}


	}
	public void Start () {

		LoadStats ();

	}

	void Update (){

		healthdebug = health;
		StatLoseSituation ();
		SaveStats ();
	}



	public void StatLoseSituation(){

		if (health <= 0) {
			LevelManager.loadLevelStatic ("DeathLose");
			ResetAllData ();
		}
	}


	public static void SaveStats(){

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");

		PlayerData data = new PlayerData ();
		data.home = home;
		data.money = money;
		data.health = health;
		data.mood = mood;
		data.energy = energy;

		bf.Serialize (file,data);
		file.Close ();

	}

	public void LoadStats(){

		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat",FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close();
			home = data.home;
			money = data.money;
			health = data.health;
			mood = data.mood;
			energy = data.energy;

		} else {
			
			Debug.Log ("save data not found setting defaults");
			home = "Tent Home";
			money = 30;
			health = 100;
			mood = 100;
			energy = 100;


		}
	}


	public static void ResetAllData (){

		Debug.Log ("restting all the stats to defaults");
		home = "Tent Home";
		money = 30;
		health = 100;
		mood = 100;
		energy = 100;

	}
		

}


[Serializable]
class PlayerData
{
	public string home;
	public float money;
	public float health;
	public float mood;
	public float energy;


}
