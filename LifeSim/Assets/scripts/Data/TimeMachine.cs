﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeMachine : MonoBehaviour {

	public float timeTravelSpeed;
	public static float minuteDTime;
	public static float hourDTime;
	public float testalpha;

	public Image timeLight;

	// Use this for initialization
	void Start () {
		timeTravelSpeed = 2.5f;
	}
	
	// Update is called once per frame
	void Update () {


		// set lighdim
	
		TimeLightSet ();

		//minute and hour time
		minuteDTime += (Time.deltaTime/3)* timeTravelSpeed;

		//resetting minute time
		if (minuteDTime > 59) {
			minuteDTime = 0;
			hourDTime += 1;
			Stats.health -= 2;
			Stats.mood -= 0.5f;
			Stats.energy -= 0.2f;
			//save game
		}

		if (hourDTime >= 24) {
			hourDTime = 0;
		}

		// diminish stats

	}
		

	public void AddSleepTime (){
		
		float addSleep = SleepTimeDisplay.sleepForTime;

		if (hourDTime + addSleep > 24) {
			hourDTime = (hourDTime + addSleep) - 24;
		} else {
			hourDTime = hourDTime + addSleep;

		}

		SleepTimeDisplay.sleepForTime = 0;


		// diminish starts along with sleep

		Stats.health -= addSleep*1.5f;
		Stats.mood -= addSleep/3;
		Stats.energy -= addSleep/5;

		//save 
		Stats.SaveStats();

	}



	public void AddTime(float htime){
		
		if (hourDTime + htime > 24) {
			hourDTime = (hourDTime + htime) - 24;
		} else {
			hourDTime = hourDTime + htime;

		}


	}



	void TimeLightSet(){

		if (hourDTime >= 6 && hourDTime <= 12) {
			timeLight.canvasRenderer.SetAlpha (0.1f);
		}
		if (hourDTime >= 13 && hourDTime <= 15) {
			timeLight.canvasRenderer.SetAlpha (0.0f);
		}
		if (hourDTime >= 16 && hourDTime <= 19) {
			timeLight.canvasRenderer.SetAlpha (0.5f);
		}
		if (hourDTime >= 20 && hourDTime <= 24) {
			timeLight.canvasRenderer.SetAlpha (0.8f);
		}
		if (hourDTime >= 0 && hourDTime <= 5) {
			timeLight.canvasRenderer.SetAlpha (0.5f);
		}
	}




}
