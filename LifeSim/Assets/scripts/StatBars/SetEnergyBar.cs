﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SetEnergyBar : MonoBehaviour {

	private Image energyBarFill;
	// Use this for initialization
	void Start () {
		energyBarFill = GetComponent<Image> ();
	}

	// Update is called once per frame
	void Update () {

		//set the bar fill
		energyBarFill.fillAmount = Stats.energy/100;
	}
}
