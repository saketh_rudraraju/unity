﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SetMoodBar : MonoBehaviour {

	private Image moodBarFill;
	// Use this for initialization
	void Start () {
		moodBarFill = GetComponent<Image> ();
	}

	// Update is called once per frame
	void Update () {

		//set the bar fill
		moodBarFill.fillAmount = Stats.mood/100;
	}
}
