﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetHealthBar : MonoBehaviour {


	private Image healthBarFill;
	// Use this for initialization
	void Start () {
		healthBarFill = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {

		//set the bar fill
		healthBarFill.fillAmount = Stats.health/100;
	}
}
