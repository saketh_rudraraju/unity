﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectController : MonoBehaviour {

	public GameObject newsPaperScreen;
	public GameObject sleepMenu;

	// alerts variables
	private static FloatingText popupText;
	private static GameObject canvas;
	// Use this for initialization
	void Start () {
		Initialize ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}




	//openclose newspaper classifieds

	public void OpenNewsPaperClassifieds(){
		
		newsPaperScreen.SetActive (true);

	}

	public void CloseNewsPaperClassifieds(){

		newsPaperScreen.SetActive (false);
	}


	//opencloase sleepmenu

	public void OpenSleepMenu(){
		if (TimeMachine.hourDTime < 5 | TimeMachine.hourDTime > 18) {
			sleepMenu.SetActive (true);
		} else {
			CreateFLoatingText ("Can't Sleep", transform);
		}
	}


	public void CloseSleepMenu (){
		sleepMenu.SetActive (false);
	}




	// everything here on are for alerts untill next string

	public static void Initialize(){

		canvas = GameObject.Find ("Canvas");
		popupText = Resources.Load <FloatingText> ("Prefabs/FloatingTextParent");
		Debug.Log ("initialize is working");

	}

	public static void CreateFLoatingText(string text, Transform location){

		FloatingText instance = Instantiate (popupText);
		instance.transform.SetParent (canvas.transform,false);
		instance.SetAlertText (text);

	}






}
